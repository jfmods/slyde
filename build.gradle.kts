plugins {
    id("jfmod") version "1.6-SNAPSHOT"
}

loom {
    accessWidenerPath.set(file("src/client/resources/slyde.accesswidener"))
}

allprojects { group = "io.gitlab.jfronny" }
base.archivesName = "slyde"

jfMod {
    minecraftVersion = "1.21.4"
    yarn("build.1")
    loaderVersion = "0.16.9"
    libJfVersion = "3.18.3"
    fabricApiVersion = "0.110.5+1.21.4"

    modrinth {
        projectId = "slyde"
        requiredDependencies.add("libjf")
        optionalDependencies.add("modmenu")
    }
    curseforge {
        projectId = "411386"
        requiredDependencies.add("libjf")
        optionalDependencies.add("modmenu")
    }
}

repositories {
    maven("https://api.modrinth.com/maven")
}

dependencies {
    modCompileOnly("maven.modrinth:sodium:mc1.21.3-0.6.1-fabric")

    modImplementation("io.gitlab.jfronny.libjf:libjf-config-core-v2")

    // For testing in dev environment
    modLocalRuntime("net.fabricmc.fabric-api:fabric-api")
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil")
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-config-ui-tiny")
    modLocalRuntime("com.terraformersmc:modmenu:12.0.0-beta.1")
//    modLocalRuntime("maven.modrinth:sodium:mc1.21.3-0.6.1-fabric")
}
