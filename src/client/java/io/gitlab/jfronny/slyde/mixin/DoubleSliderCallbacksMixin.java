package io.gitlab.jfronny.slyde.mixin;

import com.mojang.datafixers.util.Either;
import com.mojang.serialization.*;
import net.minecraft.client.option.*;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.*;

import java.util.*;

@Mixin(SimpleOption.DoubleSliderCallbacks.class)
public class DoubleSliderCallbacksMixin {
    @Redirect(method = "codec", at = @At(value = "INVOKE", target = "Lcom/mojang/serialization/Codec;doubleRange(DD)Lcom/mojang/serialization/Codec;", remap = false))
    private Codec<Double> slyde$returnRangelessCodec(double minInclusive, double maxInclusive) {
        return Codec.either(Codec.DOUBLE, Codec.BOOL)
                .xmap(either -> either.map(value -> value, value -> value ? 1.0 : 0.0), Either::left);
    }

    @Inject(method = "validate(Ljava/lang/Double;)Ljava/util/Optional;", at = @At("HEAD"), cancellable = true)
    private void slyde$skipValidate(Double double_, CallbackInfoReturnable<Optional<Double>> cir) {
        cir.setReturnValue(Optional.of(double_));
    }
}
