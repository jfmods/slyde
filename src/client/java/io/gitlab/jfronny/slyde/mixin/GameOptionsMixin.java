package io.gitlab.jfronny.slyde.mixin;

import com.mojang.serialization.DataResult;
import io.gitlab.jfronny.commons.ref.R;
import net.minecraft.client.*;
import net.minecraft.client.option.*;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;

import java.util.*;
import java.util.function.*;

@Mixin(targets = "net.minecraft.client.option.GameOptions$2")
public class GameOptionsMixin {
    @Redirect(method = "accept", at = @At(value = "INVOKE", target = "Lcom/mojang/serialization/DataResult;ifSuccess(Ljava/util/function/Consumer;)Lcom/mojang/serialization/DataResult;", remap = false))
    private <T> DataResult<T> slyde$replaceSetter(DataResult<T> instance, Consumer<T> consumer, String key, SimpleOption<T> option) {
        return instance.ifSuccess(realValue -> {
            if (option.getCallbacks() instanceof SimpleOption.SliderCallbacks) {
                if (!MinecraftClient.getInstance().isRunning()) {
                    option.value = realValue;
                    return;
                }
                if (!Objects.equals(option.getValue(), realValue)) {
                    option.value = realValue;
                    option.changeCallback.accept(realValue);
                }
                return;
            }
            consumer.accept(realValue);
        });
    }
}
