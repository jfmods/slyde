package io.gitlab.jfronny.slyde.mixin;

import com.mojang.serialization.*;
import net.minecraft.client.option.*;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.*;

import java.util.*;

@Mixin(SimpleOption.ValidatingIntSliderCallbacks.class)
public class ValidatingIntSliderCallbacksMixin {
    @Inject(method = "codec()Lcom/mojang/serialization/Codec;", at = @At("HEAD"), cancellable = true)
    private void slyde$returnRangelessCodec(CallbackInfoReturnable<Codec<Integer>> cir) {
        cir.setReturnValue(Codec.INT);
    }

    @Inject(method = "validate(Ljava/lang/Integer;)Ljava/util/Optional;", at = @At("HEAD"), cancellable = true)
    private void slyde$skipValidate(Integer integer, CallbackInfoReturnable<Optional<Integer>> cir) {
        cir.setReturnValue(Optional.of(integer));
    }
}
