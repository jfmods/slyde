package io.gitlab.jfronny.slyde.mixin;

import net.minecraft.client.option.SimpleOption;
import net.minecraft.util.math.MathHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(targets = "net.minecraft.client.option.SimpleOption$IntSliderCallbacks")
public interface IntSliderCallbackMixin extends SimpleOption.SliderCallbacks<Integer> {
    @Shadow int minInclusive();
    @Shadow int maxInclusive();

    /**
     * @author JFronny
     * @reason prevent toValue from limiting us to the upper bound
     */
    @Override
    @Overwrite
    default Integer toValue(double sliderProgress) {
        if (sliderProgress >= 1.0 && sliderProgress < 1.00001f) {
            sliderProgress = 0.99999f;
        }
        return MathHelper.floor(MathHelper.map(sliderProgress, 0.0, 1.0, this.minInclusive(), this.maxInclusive() + 1.0));
    }
}
