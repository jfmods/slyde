package io.gitlab.jfronny.slyde;

import io.gitlab.jfronny.libjf.config.api.v2.Entry;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;

@JfConfig
public class Cfg {
    @Entry public static boolean sodiumCompat = true;
    @Entry public static boolean forceSodiumCompat = false;
    @Entry public static boolean holdKeyToActivate = false;

    static {
        JFC_Cfg.ensureInitialized();
    }
}
