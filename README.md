Slyde allow setting values for options that were not intended by mojang (for example: ridiculous fov effects or increased gamma)

This will (obviously) break things if you are not careful (for example: changing the target resolution outside its slider will cause a crash)

To achieve this result, Slyde not only modifies the widgets, but also the relevant options code using a solution based on [FiveOneFourOneEight by apple502j](https://github.com/apple502j/FiveOneFourOneEight)
